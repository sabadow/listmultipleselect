package es.sabadowdev.example.selector;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MyAdapter extends BaseAdapter {

	private Context context;

	private String[] items = new String[] { "uno", "dos", "tres", "cuatro",
			"cinco", "seis", "siete" };

	public MyAdapter(Context context) {
		this.context = context;
	}

	@Override
	public int getCount() {
		return items.length;
	}

	@Override
	public String getItem(int position) {
		return items[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		MyLinear row = (MyLinear) inflater.inflate(R.layout.row,
				parent, false);

		TextView tv1 = (TextView) row.findViewById(R.id.tv1);
		tv1.setText(getItem(position));

		return row;
	}

}
